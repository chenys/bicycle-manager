import React from 'react';
import { Card } from 'antd';
import BaseForm from './../../components/BaseForm';
import axios from './../../axios';

class BikeMap extends React.Component{
    state = {
        total_count:0
    }

    map = null;

    formList = [
        {
            type:'城市'
        },{
            type:'时间查询'
        },{
            type:'SELECT',
            label:'订单状态',
            field:'order_status',
            placeholder:'全部',
            initialValue:'0',
            list:[{id:'0',name:'全部'},{id:'1',name:'进行中'},{id:'2',name:'行程结束'}]
        }
    ];

    componentDidMount(){
        this.requestList();
    }

    // 获取车辆地图数据
    requestList = () => {
        axios.ajax({
            url:'/map/bike_list',
            data:{
                params:this.params
            }
        }).then((res)=>{
            if(res.code == 0){
                this.setState({
                    total_count:res.result.total_count
                });
                this.renderMap(res.result);
            }
        });
    }

    // 查询表单
    handleFilterSubmit = (filterParams) => {
        this.params = filterParams;
        this.requestList();
    }

    // 渲染地图数据
    renderMap = (result) => {
        let list = result.route_list;
        this.map = new window.BMap.Map('container');

        // 设置起点坐标和终点坐标
        let gps1 = list[0].split(',');
        let startPoint = new window.BMap.Point(gps1[0],gps1[1]);

        let gps2 = list[list.length-1].split(',');
        let endPoint = new window.BMap.Point(gps2[0],gps2[1]);
        
        this.map.centerAndZoom(endPoint,11);    // 设置地图中心点
        this.map.enableScrollWheelZoom();       //启动鼠标滚轮缩放地图

        // 设置起点图片和终点图片
        let startPointIcon = new window.BMap.Icon('/assets/start_point.png',new window.BMap.Size(36,42),{
            imageSize:new window.BMap.Size(36,42),  // 设置图片尺寸
            anchor:new window.BMap.Size(18,42)      // 设置图片偏移量(如果不设置，图片可能没有很好的放在起点位置)
        });
        let bikeMarkerStart =new window.BMap.Marker(startPoint,{icon:startPointIcon});
        this.map.addOverlay(bikeMarkerStart);

        let endPointIcon = new window.BMap.Icon('/assets/end_point.png',new window.BMap.Size(36,42),{
            imageSize:new window.BMap.Size(36,42),  // 设置图片尺寸
            anchor:new window.BMap.Size(18,42)      // 设置图片偏移量如果不设置，图片可能没有很好的放在终点位置)
        });
        let bikeMarkerEnd =new window.BMap.Marker(endPoint,{icon:endPointIcon});
        this.map.addOverlay(bikeMarkerEnd);

        // 绘制车辆行驶路线
        let routePointList = [];
        list.forEach((item)=>{
            let p = item.split(',');
            routePointList.push(new window.BMap.Point(p[0],p[1]));
        });
        let routePolyLine = new window.BMap.Polyline(routePointList,{
            strokeColor:'#ef4136',
            strokeWeight:2,
            strokeOpacity:1
        });
        this.map.addOverlay(routePolyLine);

        // 绘制服务区
        let serviceList = result.service_list;
        let servicePointList =[];

        serviceList.forEach((item)=>{
            servicePointList.push(new window.BMap.Point(item.lon,item.lat));
        });
        let servicePolyLine = new window.BMap.Polyline(servicePointList,{
            strokeColor:'#ef4136',
            strokeWeight:3,
            strokeOpacity:1
        });
        this.map.addOverlay(servicePolyLine);

        // 添加地图中的自行车图标
        let bikeIcon = new window.BMap.Icon('/assets/bike.jpg',new window.BMap.Size(36,42),{
            imageSize:new window.BMap.Size(36,42),  // 设置图片尺寸
            anchor:new window.BMap.Size(18,42)      // 设置图片偏移量(如果不设置，图片可能没有很好的放在对应位置)
        });

        result.bike_list.forEach((item)=>{
            let p = item.split(',');
            let bikePoint = new window.BMap.Point(p[0],p[1]);
            let bikeMarkerPoint =new window.BMap.Marker(bikePoint,{icon:bikeIcon});
            this.map.addOverlay(bikeMarkerPoint);
        });
    }

    render(){
        return (
            <div>
                <Card>
                    <BaseForm formList = {this.formList} filterSubmit={this.handleFilterSubmit}/>
                </Card>
                <Card style={{marginTop:10}}>
                    <div>共{this.state.total_count}辆车</div>
                    <div id="container" style={{height:500}}></div>
                </Card>
            </div>
        );
    }
}

export default BikeMap;