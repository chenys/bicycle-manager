import React from 'react';
import { Card, Button, Modal } from 'antd';
import draftToHtml from 'draftjs-to-html';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

class RichText extends React.Component{
    state = {
        editorState:'',     // 富文本编辑器的状态
        showRichText:false, // 是否显示富文本的text内容
        contentState:'',    // 富文本的内容的状态
    }

    // 获取富文本编辑器的状态
    onEditorStateChange = (editorState) => {
        this.setState({
            editorState
        });
    }

    // 清空富文本编辑器的内容
    handleClearContent = () => {
        this.setState({
            editorState:''
        });
    }

    // 显示富文本的text内容
    handleGetText = () => {
        this.setState({
            showRichText:true
        });
    }

    // 获取富文本编辑器的内容的状态
    onEditorContentChange = (contentState) => {
        this.setState({
            contentState
        });
    }

    render(){
        const { editorState } = this.state;
        return (
            <div>
                <Card>
                    <Button type="primary" onClick={this.handleClearContent}>清空内容</Button>
                    <Button type="primary" onClick={this.handleGetText} style={{marginLeft:10}}>获取HTML文本</Button>
                </Card>
                <Card title="富文本编辑器">
                    <Editor
                        editorState={editorState}
                        onContentStateChange={this.onEditorContentChange}
                        onEditorStateChange={this.onEditorStateChange}
                    />
                </Card>
                <Modal
                    title="富文本"
                    visible={this.state.showRichText}
                    onCancel={()=>{
                        this.setState({
                            showRichText:false
                        })
                    }}
                    footer={null}
                >
                    {draftToHtml(this.state.contentState)}
                </Modal>
            </div>
        );
    }
}

export default RichText;