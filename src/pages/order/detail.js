import React from 'react';
import { Card } from 'antd';
import axios from './../../axios';
import './detail.less';

export default class Detail extends React.Component{

    state = {
        orderInfo:{},   // 订单详情
    }

    componentDidMount(){
        let orderId = this.props.match.params.orderId;
        if(orderId){
            this.getDetailInfo(orderId);
        }
    }

    // 获取订单详情
    getDetailInfo = (orderId) => {
        axios.ajax({
            url:'order/detail',
            data:{
                params:{
                    orderId
                }
            }
        }).then((res)=>{
            if(res.code==0){
                this.setState({
                    orderInfo:res.result
                });
                this.renderMap(res.result);
            }
        })
    }
    
    // 初始化百度地图
    renderMap = (result) => {
        // 创建地图实例 
        // this.map = new BMap.Map("orderDetailMap",{enableMapClick:false}); // enableMapClick:false => 无法点击地图
        // 注意：这里和百度官网的介绍有所不同，需要把BMap对象window对象下，否则会提示无法找到BMap
        // 这个是由于React是单页面程序，理论上必须在js文件开头import相关组件再使用的框架结构而造成的
        // 如果是一个多页面系统，就没有这个问题
        this.map = new window.BMap.Map("orderDetailMap");

        // 创建点坐标  
        // var point = new BMap.Point(116.404, 39.915);
        // 如果直接写城市名(比如：北京)，这里的point就不需要用到了
       
        // 初始化地图，设置中心点坐标和地图级别 => 这个放在drawBikeRoute里面做
        // this.map.centerAndZoom('北京', 11); // 第二个参数：缩放等级

        // 添加地图控件
        this.addMapControl();

        // 绘制用户的行驶轨迹
        this.drawBikeRoute(result.position_list);

        // 绘制服务区
        this.drawServiceArea(result.area);
    }

    // 添加地图控件
    addMapControl = () => {
        let map = this.map;
        map.addControl(new window.BMap.ScaleControl({anchor:window.BMAP_ANCHOR_TOP_RIGHT}));    
        map.addControl(new window.BMap.NavigationControl({anchor:window.BMAP_ANCHOR_TOP_RIGHT}));    
    }

    // 绘制用户的行驶轨迹
    drawBikeRoute = (positionList) => {
        let map = this.map;
        let startPoint = '';
        let endPoint = '';

        if(positionList.length>0){
            // 设置起始坐标点 和 结束坐标点
            let start = positionList[0];
            let end = positionList[positionList.length-1];

            startPoint = new window.BMap.Point(start.lon,start.lat);
            endPoint = new window.BMap.Point(end.lon,end.lat);

            // 设置起始图片 和 结束图片
            let startIcon = new window.BMap.Icon('/assets/start_point.png',new window.BMap.Size(36,42),{
                imageSize:new window.BMap.Size(36,42),
                anchor:new window.BMap.Size(36,42)
            });
            let endIcon = new window.BMap.Icon('/assets/end_point.png',new window.BMap.Size(36,42),{
                imageSize:new window.BMap.Size(36,42),
                anchor:new window.BMap.Size(36,42)
            });

            // 通过Marker把起始图片放到起始坐标点，把结束图片放到结束坐标点
            let startMarker = new window.BMap.Marker(startPoint,{icon:startIcon});
            let endMarker = new window.BMap.Marker(endPoint,{icon:endIcon});

            // 通过addOverlay把startMarker和endMarker显示在地图上
            this.map.addOverlay(startMarker);
            this.map.addOverlay(endMarker);

            // 连接路线图
            let trackPoint = [];
            positionList.map(v=>{
                trackPoint.push(new window.BMap.Point(v.lon,v.lat));
            });

            // 画把所有点连接起来之后的折线
            let polyline = new window.BMap.Polyline(trackPoint,{
                strokeColor:'#1869AD',
                strokeWeight:3,
                strokeOpacity:1
            });
            this.map.addOverlay(polyline);

            // 设置中心点坐标和地图级别 -> 这里把结束点作为地图的中心点
            this.map.centerAndZoom(endPoint, 11);
        }
    }

    // 绘制服务区
    drawServiceArea = (area) => {
        let servicePoint = [];
        area.map(v=>{
            servicePoint.push(new window.BMap.Point(v.lon,v.lat));
        });

        // 画把所有点连接起来之后的多边形
        let polygon = new window.BMap.Polygon(servicePoint,{
            strokeColor:'#CE0000',
            strokeWeight:4,
            strokeOpacity:1,
            fillColor:'#ff8605',
            fillOpacity:0.4
        })
        this.map.addOverlay(polygon);
    }

    render(){
        const { orderInfo } = this.state;

        return (
            <div>
                <Card>
                    <div id="orderDetailMap" className="order-map"></div>
                    <div className="detail-items">
                        <div className="item-title">基础信息</div>
                        <ul className="detail-form">
                            <li>
                                <div className="detail-form-left">用车模式</div>
                                <div className="detail-form-content">{orderInfo.mode == 1 ?'服务区':'停车点'}</div>    
                            </li>
                            <li>
                                <div className="detail-form-left">订单编号</div>
                                <div className="detail-form-content">{orderInfo.order_sn}</div>    
                            </li>
                            <li>
                                <div className="detail-form-left">车辆编号</div>
                                <div className="detail-form-content">{orderInfo.bike_sn}</div>    
                            </li>
                            <li>
                                <div className="detail-form-left">用户姓名</div>
                                <div className="detail-form-content">{orderInfo.user_name}</div>    
                            </li>
                            <li>
                                <div className="detail-form-left">手机号码</div>
                                <div className="detail-form-content">{orderInfo.mobile}</div>    
                            </li>
                        </ul>
                    </div>
                    <div className="detail-items">
                        <div className="item-title">行驶轨迹</div>
                        <ul className="detail-form">
                            <li>
                                <div className="detail-form-left">行程起点</div>
                                <div className="detail-form-content">{orderInfo.start_location}</div>    
                            </li>
                            <li>
                                <div className="detail-form-left">行程终点</div>
                                <div className="detail-form-content">{orderInfo.end_location}</div>    
                            </li>
                            <li>
                                <div className="detail-form-left">行驶里程</div>
                                <div className="detail-form-content">{orderInfo.distance/1000}公里</div>    
                            </li>
                        </ul>
                    </div>
                </Card>
            </div>
        );
    }
}