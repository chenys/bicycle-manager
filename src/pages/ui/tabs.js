import React from 'react';
import { Card, Tabs, message, Icon } from 'antd';
import './ui.less'
const TabPane = Tabs.TabPane;

class Tab extends React.Component{

    // 需要定义，否则无法正常新增和删除Tab
    newTabIndex = 0;

    componentWillMount(){
        const panes = [
            {
                title:"tab1",
                content:"tab 1",
                key:"1"
            },
            {
                title:"tab2",
                content:"tab 2",
                key:"2"
            },
            {
                title:"tab3",
                content:"tab 3",
                key:"3"
            }
        ];
        this.setState({
            panes,
            activeKey:panes[0].key
        });
    }

    // 切换Tab时的回调处理
    handleCallback = (key) => {
        message.info("Hi,您选择了页签："+key);
    }

    // 直接从antD官网中复制过来即可用
    onChange = (activeKey) => {
        this.setState({
            activeKey
        });
    }

    // 直接从antD官网中复制过来即可用
    onEdit = (targetKey, action) => {
        this[action](targetKey);
    }

    // 直接从antD官网中复制过来即可用
    add = () => {
        const panes = this.state.panes;
        const activeKey = `newTab${this.newTabIndex++}`;
        panes.push({ title: 'New Tab', content: 'Content of new Tab', key: activeKey });
        this.setState({ panes, activeKey });
    }

    // 直接从antD官网中复制过来即可用
    remove = (targetKey) => {
        let activeKey = this.state.activeKey;
        let lastIndex;
        this.state.panes.forEach((pane, i) => {
            if (pane.key === targetKey) {
                lastIndex = i - 1;
                }
            });
        const panes = this.state.panes.filter(pane => pane.key !== targetKey);
        if (lastIndex >= 0 && activeKey === targetKey) {
        activeKey = panes[lastIndex].key;
        }
        this.setState({ panes, activeKey });
    }

    render(){
        return (
            <div>
                <Card title="Tab页签" className="card-wrap">
                    <Tabs defaultActiveKey="1" onChange={this.handleCallback}>
                        <TabPane tab="Tab 1" key="1">Content of Tab Pane 1</TabPane>
                        <TabPane tab="Tab 2" key="2" disabled>Content of Tab Pane 2</TabPane>
                        <TabPane tab="Tab 3" key="3">Content of Tab Pane 3</TabPane>
                    </Tabs>
                </Card>
                <Card title="Tab带图的页签" className="card-wrap">
                    <Tabs defaultActiveKey="1" onChange={this.handleCallback}>
                        <TabPane tab={<span><Icon type="plus"/>Tab 1</span>} key="1">Content of Tab Pane 1</TabPane>
                        <TabPane tab={<span><Icon type="edit"/>Tab 2</span>} key="2">Content of Tab Pane 2</TabPane>
                        <TabPane tab={<span><Icon type="delete"/>Tab 3</span>} key="3">Content of Tab Pane 3</TabPane>
                    </Tabs>
                </Card>
                <Card title="Tab可编辑的页签" className="card-wrap">
                    <Tabs
                        type="editable-card"
                        activeKey={this.state.activeKey}
                        onChange={this.onChange}
                        onEdit={this.onEdit}
                    >
                        {
                            this.state.panes.map((panel)=>{
                                return <TabPane
                                    tab={panel.title}
                                    key={panel.key}
                                >
                                    {panel.content}
                                </TabPane>
                            })
                        }
                    </Tabs>
                </Card>
            </div>
        );
    }
}

export default Tab;