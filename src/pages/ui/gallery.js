import React from 'react';
import { Card, Row, Col, Modal } from 'antd';
import './ui.less';

class Gallery extends React.Component{
    state = {
        currentImg:null,
        visible:false
    }

    // 显示模态框
    openGallery = (currentImg) => {
        this.setState({
            currentImg,
            visible:true
        });
    }

    // 隐藏模态框
    handleCancel = () => {
        this.setState({
            visible: false
        })
    }

    render(){
        // 定义画廊图片
        const imgs = [];
        for(let i=1;i<=6;i++){
            const items = [];
            for(let j=1;j<=4;j++){
                items.push(`${4*(i-1)+j}.png`);
            }
            imgs.push(items);
        }

        // 使用Card的cover属性制作画廊
        const imgList = imgs.map((list,i) => list.map((item,j) => 
            <Card
                style={{marginBottom:8}}
                cover={<img src={'/gallery/'+item} alt="图片" onClick={()=>this.openGallery(item)}/>}
                key={4*(i-1)+j}
            >
                <Card.Meta
                    title="React Admin"
                    description="I Love Imooc"
                />
            </Card>
        ));

        return (
            <div className="card-wrap">
                <Row gutter={8}>
                    <Col md={4}>
                        {imgList[0]}
                    </Col>
                    <Col md={4}>
                        {imgList[1]}
                    </Col>
                    <Col md={4}>
                        {imgList[2]}
                    </Col>
                    <Col md={4}>
                        {imgList[3]}
                    </Col>
                    <Col md={4}>
                        {imgList[4]}
                    </Col>
                    <Col md={4}>
                        {imgList[5]}
                    </Col>
                </Row>
                <Modal
                    title={"图片画廊"}
                    visible={this.state.visible}
                    onCancel={() => this.handleCancel()}
                    footer={null}
                >
                    <img src={`/gallery/`+this.state.currentImg} alt="" style={{width:"100%"}}/>
                </Modal>
            </div>
        );
    }
}

export default Gallery;