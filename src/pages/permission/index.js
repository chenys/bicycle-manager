import React from 'react';
import { Card, Button, Form, Select, Modal, Input, Message, Tree, Transfer } from 'antd';
import ETable from './../../components/ETable';
import Utils from './../../utils/utils';
import axios from './../../axios';
import menuList from './../../config/menuConfig';

const Option = Select.Option;
const FormItem = Form.Item;
const TreeNode = Tree.TreeNode;

class PermissionUser extends React.Component{
    state = {
        list:[],            // 表格信息
        pagination:null,    // 分页信息
        isRoleVisible:false,// 是否显示创建角色的模态框
        isPermVisible:false,// 是否显示设置权限的模态框
        isUserVisible:false,// 是否显示用户授权的模态框
        selectedRowKeys:[], // 单选框中被选中行的keys
        selectedRows:[],    // 单选框中被选中行的内容
        detailInfo:{},      // 当前选中角色的信息
        dataSource:[],      // 穿梭框的数据源
        targetKeys:[],      // 穿梭框的目标键值
    }

    params = {
        page:1
    }

    componentWillMount(){
        this.requestList();
    }

    // 取得列表数据
    requestList = () => {
        axios.requestList(this,'/role/list',this.params);
    }

    // 打开创建角色弹框
    handleCreateRole = () => {
        this.setState({
            isRoleVisible:true
        });
    }

    // 创建角色提交
    handleRoleSubmit = () => {
        let data = this.roleForm.props.form.getFieldsValue();
        axios.ajax({
            url:'/role/create',
            data:{
                params:data
            }
        }).then((res)=>{
            if(res.code == 0){
                this.roleForm.props.form.resetFields();
                this.setState({
                    isRoleVisible:false
                });
                this.requestList();
                Message.success('创建成功');
            }
        })
    }

    // 打开设置权限弹框
    handlePermission = () => {
        let item = this.state.selectedRows[0];
        if(!item){
            Modal.info({
                title:'信息',
                content:'请先选择一个角色'
            });
            return;
        }
        this.setState({
            isPermVisible:true,
            detailInfo:item
        });
    }

    // 设置权限提交
    handlePermSubmit = () => {
        let data = this.permForm.props.form.getFieldsValue();
        data.role_id = this.state.selectedRows[0].id;
        data.menus = this.state.detailInfo.menus;

        axios.ajax({
            url:'/perm/edit',
            data:{
                params:data
            }
        }).then((res)=>{
            if(res.code == 0){
                this.setState({
                    isPermVisible:false
                });
                this.requestList();
                Message.success('设置成功');
            }
        })
    }

    // 打开用户授权弹框
    handleUserAuth = () =>{
        let item = this.state.selectedRows[0];
        if(!item){
            Modal.info({
                title:'信息',
                content:'请先选择一个角色'
            });
            return;
        }
        this.getRoleUserList(item.id);
        this.setState({
            detailInfo:item,
            isUserVisible:true
        });
    }

    // 获得该角色的用户信息
    getRoleUserList = (id) => {
        axios.ajax({
            url:'/role/user_list',
            data:{
                params:{
                    id
                }
            }
        }).then((res)=>{
            if(res.code == 0){
                this.getAuthUserList(res.result);
            }
        });
    }

    // 筛选目标用户
    getAuthUserList = (result) => {
        const dataSource = [];
        const targetKeys = [];
        if(result && result.length>0){
            result.map(item=>{
                const data = {
                    key:item.user_id,
                    title:item.user_name,
                    status:item.status
                }
                if(item.status == 1){
                    targetKeys.push(data.key);
                }
                dataSource.push(data);
            });
            
            this.setState({
                dataSource,
                targetKeys
            });
        }
    }

    // 用户授权提交
    handleUserSubmit = () => {
        let data = {};
        data.user_id = this.state.targetKeys;
        data.role_id = this.state.selectedRows[0].id;
        axios.ajax({
            url:'/role/user_role_edit',
            data:{
                params:{
                    ...data
                }
            }
        }).then((res)=>{
            if(res.code == 0){
                this.setState({
                    isUserVisible:false
                })
                this.requestList();
                Message.success('授权成功');
            }
        })
    }
    
    render(){
        const columns = [
            {
                title:'角色ID',
                dataIndex:'id'
            },
            {
                title:'角色名称',
                dataIndex:'role_name'
            },
            {
                title:'创建时间',
                dataIndex:'create_time',
                render:Utils.formateDate
            },
            {
                title:'使用状态',
                dataIndex:'status',
                render(status){
                    return status == 1?'启用':'停用';
                }
            },
            {
                title:'授权时间',
                dataIndex:'authorize_time',
                render:Utils.formateDate
            },
            {
                title:'授权人',
                dataIndex:'authorize_user_name'
            },
        ]

        return (
            <div>
                <Card>
                    <Button type="primary" onClick={this.handleCreateRole}>创建角色</Button>
                    <Button type="primary" onClick={this.handlePermission} style={{margin:"0 10px"}}>设置权限</Button>
                    <Button type="primary" onClick={this.handleUserAuth}>用户授权</Button>
                </Card>
                <div className="content-wrap">
                    <ETable
                        columns={columns}
                        dataSource={this.state.list}
                        pagination={this.state.pagination}
                        rowSelectionType={'radio'}
                        selectedRowKeys={this.state.selectedRowKeys}
                        selectedRows={this.state.selectedRows}
                        updateSelectedItem={Utils.updateSelectedItem.bind(this)}
                    />
                </div>
                <Modal
                    title="创建角色"
                    visible={this.state.isRoleVisible}
                    onOk={this.handleRoleSubmit}
                    onCancel={()=>{
                        this.roleForm.props.form.resetFields();
                        this.setState({
                            isRoleVisible:false
                        })
                    }}
                >
                    <RoleForm wrappedComponentRef={(inst)=>this.roleForm = inst}/>
                </Modal>
                <Modal
                    title="设置权限"
                    visible={this.state.isPermVisible}
                    onOk={this.handlePermSubmit}
                    onCancel={()=>{
                        this.setState({
                            isPermVisible:false
                        })
                    }}
                >
                    <PermForm 
                        wrappedComponentRef={(inst)=>this.permForm = inst}
                        detailInfo={this.state.detailInfo}
                        patchMenuInfo={(checkedKeys)=>{
                            this.setState({
                                detailInfo:{
                                    ...this.state.detailInfo,
                                    menus: checkedKeys
                                }
                            })
                        }}
                    />
                </Modal>
                <Modal
                    title="用户授权"
                    visible={this.state.isUserVisible}
                    width={800}
                    onOk={this.handleUserSubmit}
                    onCancel={()=>{
                        this.setState({
                            isUserVisible:false
                        })
                    }}
                >
                    <UserAuthForm
                        wrappedComponentRef={(inst)=>this.userAuthForm = inst}
                        detailInfo={this.state.detailInfo}
                        targetKeys={this.state.targetKeys}
                        dataSource={this.state.dataSource}
                        patchUserInfo={(targetKeys)=>{
                            this.setState({
                                targetKeys
                            })
                        }}
                    />
                </Modal>
            </div>
        );
    }
}

class RoleForm extends React.Component{
    render(){
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol:{span:5},
            wrapperCol:{span:15}
        }
        
        return (
            <Form layout="horizontal">
                <FormItem label="角色名称" {...formItemLayout}>
                    {
                        getFieldDecorator('role_name')(
                            <Input type="text" placeholder="请输入角色名称"/>
                        )
                    }
                </FormItem>
                <FormItem label="状态" {...formItemLayout}>
                    {
                        getFieldDecorator('status')(
                            <Select>
                                <Option value={1}>开启</Option>
                                <Option value={0}>关闭</Option>
                            </Select>
                        )
                    }
                </FormItem>
            </Form>
        );
    }
}
RoleForm = Form.create({})(RoleForm);

class PermForm extends React.Component{

    renderTreeNodes = (data) => {
        return data.map((item)=>{
            if(item.children){
                return <TreeNode {...item}>
                    {this.renderTreeNodes(item.children)}
                </TreeNode>
            }else{
                return <TreeNode {...item}/>
            }
        })
    }

    onCheckNode = (checkedKeys) => {
        this.props.patchMenuInfo(checkedKeys);
    }

    render(){
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol:{span:5},
            wrapperCol:{span:15}
        }
        const detail_info  = this.props.detailInfo;

        return (
            <Form layout="horizontal">
                <FormItem label="角色名称" {...formItemLayout}>
                    <Input disabled placeholder={detail_info.role_name}/>
                </FormItem>
                <FormItem label="状态" {...formItemLayout}>
                    {
                        getFieldDecorator('status',{
                            initialValue:detail_info.status
                        })(
                            <Select>
                                <Option value={1}>启用</Option>
                                <Option value={0}>停用</Option>
                            </Select>
                        )
                    }
                </FormItem>
                <Tree
                    checkable           // 允许选中
                    defaultExpandAll    // 默认展开
                    onCheck={(checkedKeys)=>{
                        this.onCheckNode(checkedKeys)
                    }}
                    checkedKeys={detail_info.menus}
                >
                    <TreeNode title="平台权限" key="platform_all">
                        {this.renderTreeNodes(menuList)}
                    </TreeNode>
                </Tree>
            </Form>
        );
    }
}
PermForm = Form.create({})(PermForm);

class UserAuthForm extends React.Component{


    // 固定写法，用于筛选用户
    filterOption = (inputValue, option) => {
        return option.title.indexOf(inputValue) > -1;
    }

    handleChange = (targetKeys) => {
        this.props.patchUserInfo(targetKeys);
    }

    render(){
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol:{span:5},
            wrapperCol:{span:15}
        }
        const detail_info  = this.props.detailInfo;

        return (
            <Form layout="horizontal">
                <FormItem label="角色名称" {...formItemLayout}>
                    <Input disabled placeholder={detail_info.role_name}/>
                </FormItem>
                <FormItem label="选择用户" {...formItemLayout}>
                    <Transfer
                        listStyle={{width:210,height:400}}
                        dataSource={this.props.dataSource}
                        targetKeys={this.props.targetKeys}
                        titles={['待选用户','已选用户']}
                        showSearch
                        searchPlaceholder='输入用户名'
                        filterOption={this.filterOption}
                        render={item=>item.title}
                        onChange={this.handleChange}
                    />
                </FormItem>
            </Form>
        );
    }
}
UserAuthForm = Form.create({})(UserAuthForm);

export default PermissionUser;