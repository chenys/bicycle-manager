import React from 'react';
import { Card, Button, Modal, Form, Input, Radio, DatePicker, Select, Message } from 'antd';
import axios from './../../axios';
import Utils from './../../utils/utils';
import ETable from './../../components/ETable';
import BaseForm from './../../components/BaseForm';
import Wordbook from './../../config/wordbook';
import moment from 'moment';
const FormItem = Form.Item;
const RadioGroup = Radio.Group;
const TextArea = Input.TextArea;
const Option = Select.Option;

class User extends React.Component{
    state = {
        list:[],                // 表格信息
        pagination:null,        // 分页信息
        type:'',                // 操作类型
        isVisible:false,        // 是否显示模态框
        title:'',               // 模态框标题
        selectedRowKeys:[],     // 单选框中被选中行的keys
        selectedRows:[],        // 单选框中被选中行的内容
        userInfo:{},            // 被选中用户的信息
    }

    params = {
        page:1
    }

    formList = [
        {
            type:'INPUT',
            label:'用户名',
            field:'user_name',
            placeholder:'请输入用户名',
        },
        {
            type:'INPUT',
            label:'手机号',
            field:'user_mobile',
            placeholder:'请输入用户手机号',
        },
        {
            type:'DATE',
            label:'入职日期',
            field:'user_date',
            placeholder:'请选择入职日期',
        }
    ]

    componentDidMount(){
        this.requestList();
    }

    // 查询处理
    handlefilter = (params) => {
        this.params = params;
        this.requestList();
    }

    // 取得列表数据
    requestList = () => {
        axios.requestList(this,'/user/list',this.params);
    }

    // 操作员工列表
    handleOperate = (type) => {
        let item = this.state.selectedRows[0];
        let _this = this;

        if(type != 'add'){
            if(!item){
                Modal.info({
                    title:"提示",
                    content:"请选择一个用户"
                });
                return;
            }
        }

        if(type == 'add'){
            this.setState({
                type,
                isVisible:true,
                title:'创建员工'
            });
        }else if(type == 'edit'){
            this.setState({
                type,
                isVisible:true,
                title:'编辑员工',
                userInfo:item
            });
        }else if(type == 'detail'){
            this.setState({
                type,
                isVisible:true,
                title:'员工详情',
                userInfo:item
            });
        }else{
            Modal.confirm({
                title:'确认删除',
                content:'是否要删除当前选中的员工',
                onOk(){_this.handleDelete(item)}
            });
        }
    }

    // 创建员工提交
    handleSubmit = () => {
        let type = this.state.type;
        let date = this.userForm.props.form.getFieldsValue();
        axios.ajax({
            url:type=='add'?'/user/add':'/user/edit',
            data:{
                params:date
            }
        }).then((res)=>{
            if(res.code == 0){
                this.userForm.props.form.resetFields();
                this.setState({
                    isVisible:false,
                    userInfo:{}
                });
                this.requestList();
                Message.success(type=='add'?'创建成功':'编辑成功');
            }
        })
    }

    // 删除员工提交
    handleDelete = (item) => {
        axios.ajax({
            url:'/user/delete',
            data:{
                params:{
                    id:item.id
                }
            }
        }).then((res)=>{
            if(res.code == 0){
                this.setState({
                    userInfo:{},
                    selectedRowKeys:[],
                    selectedRows:[]
                });
                this.requestList();
                Message.success('删除成功');
            }
        })
    }

    render(){
        const columns = [
            {
                title:'id',
                dataIndex:'id'
            },
            {
                title:'用户名',
                dataIndex:'userName'
            },
            {
                title:'性别',
                dataIndex:'sex',
                render(sex){
                    return Wordbook.sex[sex];
                }
            },
            {
                title:'状态',
                dataIndex:'state',
                render(state){
                    return Wordbook.state[state];
                }
            },
            {
                title:'爱好',
                dataIndex:'interest',
                render(interest){
                    return Wordbook.interest[interest];
                }
            },
            {
                title:'生日',
                dataIndex:'birthday'
            },
            {
                title:'联系地址',
                dataIndex:'address'
            },
            {
                title:'早起时间',
                dataIndex:'time'
            }
        ];
        let footer = {};
        if(this.state.type == 'detail'){
            footer = {
                footer:null
            };
        }

        return (
            <div>
                <Card>
                    <BaseForm formList={this.formList} filterSubmit={this.handlefilter}/>
                </Card>
                <Card style={{marginTop:10}} className="operate-wrap">
                    <Button type="primary" icon='plus' onClick={()=>this.handleOperate('add')}>创建员工</Button>
                    <Button type="primary" icon='edit' onClick={()=>this.handleOperate('edit')}>编辑员工</Button>
                    <Button type="primary" onClick={()=>this.handleOperate('detail')}>员工详情</Button>
                    <Button type="danger" icon='delete' onClick={()=>this.handleOperate('delete')}>删除员工</Button>
                </Card>
                <div className="content-wrap">
                    <ETable
                        columns={columns}
                        dataSource={this.state.list}
                        pagination={this.state.pagination}
                        rowSelectionType={'radio'}
                        selectedRowKeys={this.state.selectedRowKeys}
                        updateSelectedItem={Utils.updateSelectedItem.bind(this)}
                    />
                </div>
                <Modal
                    title={this.state.title}
                    visible={this.state.isVisible}
                    onOk={this.handleSubmit}
                    onCancel={()=>{
                        this.userForm.props.form.resetFields();
                        this.setState({
                            isVisible:false,
                            userInfo:{}
                        })
                    }}
                    width={600}
                    {...footer}
                >
                    <UserForm type={this.state.type} userInfo={this.state.userInfo} wrappedComponentRef={(inst)=>this.userForm = inst}/>
                </Modal>
            </div>
        );
    }
}

class UserForm extends React.Component{
    render(){
        let type = this.props.type;
        let userInfo = this.props.userInfo;
        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol:{span:5},
            wrapperCol:{span:15}
        }
        
        return (
            <Form layout="horizontal">
                <FormItem label="用户名" {...formItemLayout}>
                    {
                        type == 'detail' ? userInfo.userName :
                        getFieldDecorator('user_name',{
                            initialValue:userInfo.userName
                        })(
                            <Input type="text" placeholder="请输入用户名"/>
                        )
                    }
                </FormItem>
                <FormItem label="性别" {...formItemLayout}>
                    {
                        type == 'detail' ? Wordbook.sex[userInfo.sex] :
                        getFieldDecorator('sex',{
                            initialValue:userInfo.sex
                        })(
                            <RadioGroup>
                                <Radio value={1}>男</Radio>
                                <Radio value={2}>女</Radio>
                            </RadioGroup>
                        )
                    }
                </FormItem>
                <FormItem label="状态" {...formItemLayout}>
                    {
                        type == 'detail' ? Wordbook.state[userInfo.state] :
                        getFieldDecorator('state',{
                            initialValue:userInfo.state
                        })(
                            <Select>
                                <Option value={1}>咸鱼一条</Option>
                                <Option value={2}>风华浪子</Option>
                                <Option value={3}>北大才子一枚</Option>
                                <Option value={4}>百度FE</Option>
                                <Option value={5}>创业者</Option>
                            </Select>
                        )
                    }
                </FormItem>
                <FormItem label="生日" {...formItemLayout}>
                    {
                        type == 'detail' ? userInfo.birthday :
                        getFieldDecorator('birthday',{
                            initialValue:JSON.stringify(userInfo) == '{}'?null:moment(userInfo.birthday)
                        })(
                            <DatePicker placeholder="请选择生日"/>
                        )
                    }
                </FormItem>
                <FormItem label="联系地址" {...formItemLayout}>
                    {
                        type == 'detail' ? userInfo.address :
                        getFieldDecorator('address',{
                            initialValue:userInfo.address
                        })(
                            <TextArea rows={3} placeholder="请输入联系地址"/>
                        )
                    }
                </FormItem> 
            </Form>
        );
    }
}
UserForm = Form.create({})(UserForm);

export default User;