import React from 'react';
import { Card } from 'antd';

// 按需加载，导入核心组件
import echarts from 'echarts/lib/echarts';
// 按需加载，导入饼图组件
import 'echarts/lib/chart/line';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/markPoint';
// 导入主题
import echartsTheme from './../echartTheme';
import ReactEcharts from 'echarts-for-react';

class Line extends React.Component{

    componentWillMount(){
        // 注册主题
        echarts.registerTheme('Imooc',echartsTheme);
    }

    getOption = () => {
        let option = {
            title:{
                text:'用户骑行订单'
            },
            tooltip:{
                trigger:'axis'
            },
            xAxis:{
                data:['周一','周二','周三','周四','周五','周六','周日']
            },
            yAxis:{
                type:'value'
            },
            series:[
                {
                    name:'订单量',
                    type:'line',
                    data:[1000,2000,1500,3000,2000,1200,800]
                },
            ]
        }

        return option;
    }

    getOption2 = () => {
        let option = {
            title:{
                text:'用户骑行订单'
            },
            tooltip:{
                trigger:'axis'
            },
            legend:{
                data:['OFO订单量','摩拜订单量']
            },
            xAxis:{
                data:['周一','周二','周三','周四','周五','周六','周日']
            },
            yAxis:{
                type:'value'
            },
            series:[
                {
                    name:'OFO订单量',
                    type:'line',
                    data:[2000,3000,5500,7000,8000,12000,20000]
                },
                {
                    name:'摩拜订单量',
                    type:'line',
                    data:[1500,3000,4500,6000,8000,10000,15000]
                },
            ]
        }

        return option;
    }

    getOption3 = () => {
        let option = {
            title:{
                text:'用户骑行订单'
            },
            tooltip:{
                trigger:'axis'
            },
            xAxis:{
                boundaryGap: false,
                data:['周一','周二','周三','周四','周五','周六','周日']
            },
            yAxis:{
                type:'value'
            },
            series:[
                {
                    name:'订单量',
                    type:'line',
                    areaStyle: {},
                    data:[1000,2000,1500,3000,2000,1200,800]
                },
            ]
        }

        return option;
    }

    render(){
        return (
            <div>
                <Card title="折线图表之一">
                    <ReactEcharts option={this.getOption()} theme="Imooc" style={{height:500}}/>
                </Card>
                <Card title="折线图表之二" style={{marginTop:10}}>
                    <ReactEcharts option={this.getOption2()} theme="Imooc" style={{height:500}}/>
                </Card>
                <Card title="折线图表之三" style={{marginTop:10}}>
                    <ReactEcharts option={this.getOption3()} theme="Imooc" style={{height:500}}/>
                </Card>
            </div>
        );
    }
}

export default Line;