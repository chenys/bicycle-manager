import React from 'react';
import { Link } from 'react-router-dom';

export default class Home extends React.Component{
    render(){
        return (
            <div>
                <ul>
                    <li>
                        <Link to="/">Main3</Link>
                    </li>
                    <li>
                        <Link to="/about/chen">About3</Link>
                    </li>
                    <li>
                        <Link to="/topic/xiao">Topic3</Link>
                    </li>
                    <li>
                        <Link to="/imooc">imooc</Link>
                    </li>
                </ul>
                <hr/>
                {this.props.children}
            </div>
        );
    }
}