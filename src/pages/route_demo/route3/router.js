import React from 'react';
import {HashRouter as Router,Route,Switch } from 'react-router-dom';
import Main from './Main';
import About from './About';
import Topic from './Topic';
import Home from './Home';
import NoMatch from './NoMatch';

export default class IRouter extends React.Component{
    render(){
        return (
            <Router>
                <Home>
                    <Switch>
                        <Route exact path="/" component={Main}></Route>
                        <Route path="/about/:id" component={About}></Route>
                        <Route path="/topic/:uid" component={Topic}></Route>
                        <Route component={NoMatch}></Route>
                    </Switch>
                </Home>
            </Router>
        );
    }
}