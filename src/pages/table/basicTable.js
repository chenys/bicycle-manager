import React from 'react';
import { Card, Table, Modal, Button, message } from 'antd';
import axios from './../../axios/index';
import utils from './../../utils/utils';

class BasicTable extends React.Component{

    state = {
        dataSource:[],              // 数据源1
        dataSource2:[],             // 数据源2
        selectedRowRadioKeys:[],    // 单选框中被选中行的keys
        selectedRadioRows:[],       // 单选框中被选中行的内容
        selectedRowCheckKeys:[],    // 复选框中被选中的keys
        selectedCheckRows:[],       // 复选框中被选中行的内容
        pagination:null,            // 分页对象
    }

    params = {
        page:1
    }

    componentDidMount(){
        const dataSource = [
            {
                id:'0',
                userName:'Jack',
                sex:'1',
                state:'1',
                interest:'1',
                birthday:'2000-01-01',
                address:'杭州市滨江区星光大道',
                time:'09:00'
            },
            {
                id:'1',
                userName:'Tom',
                sex:'1',
                state:'1',
                interest:'1',
                birthday:'2000-01-01',
                address:'杭州市滨江区星光大道',
                time:'09:00'
            },
            {
                id:'2',
                userName:'Lili',
                sex:'1',
                state:'1',
                interest:'1',
                birthday:'2000-01-01',
                address:'杭州市滨江区星光大道',
                time:'09:00'
            },
        ]
        // 为表格的每一行添加key
        dataSource.map((item,index)=>{
            item.key = index;
        });
        this.setState({
            dataSource
        });
        this.request();
    }

    // 动态获取mock数据
    request = ()=>{
        let _this = this;
        axios.ajax({
            url:'/table/list',
            data:{
                params:{
                    page:this.params.page
                },
                // isShowLoading:false
            }
        }).then((res)=>{
            if(res.code === 0){
                // 为表格的每一行添加key
                res.result.list.map((item,index)=>{
                    item.key = index;
                });
                this.setState({
                    dataSource2:res.result.list,
                    selectedRowRadioKeys:[],
                    selectedRadioRows:[],
                    selectedRowCheckKeys:[],
                    selectedCheckRows:[],
                    pagination:utils.pagination(res,(current)=>{
                        _this.params.page = current;
                        this.request();
                    })
                });
            }
        });
    }

    // 点击Mock-单选表格的行，保存相应的数据
    handleRadioRowClick = (record,index) => {
        let selectKey = [index];
        this.setState({
            selectedRowRadioKeys:selectKey,
            selectedRadioRows:record
        });
    }

    // 点击Mock-复选表格的行，保存相应的数据
    handleCheckRowClick = (record,index) => {
        let selectedRowCheckKeys = this.state.selectedRowCheckKeys;
        let selectedCheckRows = this.state.selectedCheckRows;

        if(selectedRowCheckKeys.length === 0){
            selectedRowCheckKeys.push(index);
            selectedCheckRows.push(record);
        }else{
            let pos = -1;
            selectedRowCheckKeys.map((item,idx) => {
                if(item === index){
                    pos = idx;
                }
            });

            if(pos !== -1){
                selectedRowCheckKeys.splice(pos,1);
                selectedCheckRows.splice(pos,1);
            }else{
                selectedRowCheckKeys.push(index);
                selectedCheckRows.push(record);
            }
        }
        this.setState({
            selectedRowCheckKeys,
            selectedCheckRows
        });
    }

    // 多选执行删除动作
    handleCheckDelete = () => {
        let rows = this.state.selectedCheckRows;
        let ids = rows.map(item => item.id) || [];
        Modal.confirm({
            title: '删除提示',
            content: `您确定要删除这些数据吗？${ids.join(',')}`,
            onOk:()=>{
                message.success('删除成功');
                this.request();
            }
        });

    }

    render(){
        const columns = [
            {
                title:'id',
                dataIndex:'id'
            },
            {
                title:'用户名',
                dataIndex:'userName'
            },
            {
                title:'性别',
                dataIndex:'sex',
                render(sex){
                    return sex === 1 ? '男' : '女';
                }
            },
            {
                title:'状态',
                dataIndex:'state',
                render(state){
                    let config = {
                        '1':'咸鱼一条',
                        '2':'风华浪子',
                        '3':'北大才子',
                        '4':'百度FE',
                        '5':'创业者'
                    }
                    return config[state];
                }
            },
            {
                title:'爱好',
                dataIndex:'interest',
                render(interest){
                    let config = {
                        '1':'游泳',
                        '2':'打篮球',
                        '3':'踢足球',
                        '4':'跑步',
                        '5':'爬山',
                        '6':'骑行',
                        '7':'桌球',
                        '8':'麦霸'
                    }
                    return config[interest];
                }
            },
            {
                title:'生日',
                dataIndex:'birthday'
            },
            {
                title:'地址',
                dataIndex:'address'
            },
            {
                title:'早起时间',
                dataIndex:'time'
            }
        ];
        const rowRadioSelection = {
            type:'radio',
            selectedRowKeys: this.state.selectedRowRadioKeys,

            onChange:(selectedRowKeys,selectedRows)=>{
                this.setState({
                    selectedRowRadioKeys: selectedRowKeys,
                    selectedRadioRows: selectedRows
                });
            }
        }
        const rowCheckSelection = {
            type:'checkbox',
            selectedRowKeys: this.state.selectedRowCheckKeys,

            onChange:(selectedRowKeys,selectedRows)=>{
                this.setState({
                    selectedRowCheckKeys: selectedRowKeys,
                    selectedCheckRows: selectedRows
                });
            }
        }

        return(
            <div>
                <Card title="基础表格">
                    <Table
                        bordered
                        columns={columns}
                        dataSource={this.state.dataSource}
                        pagination={false}
                    />
                </Card>
                <Card title="动态数据渲染表格-Mock" style={{margin:"10px 0"}}>
                    <Table
                        bordered
                        columns={columns}
                        dataSource={this.state.dataSource2}
                        pagination={false}
                    />
                </Card>
                <Card title="Mock-单选" style={{margin:"10px 0"}}>
                    <Table
                        bordered
                        rowSelection = {rowRadioSelection}
                        onRow={(record,index) => {
                            return {
                                onClick: () => { this.handleRadioRowClick(record,index)}
                            }
                        }}
                        columns={columns}
                        dataSource={this.state.dataSource2}
                        pagination={false}
                    />
                </Card>
                <Card title="Mock-复选" style={{margin:"10px 0"}}>
                    <div style={{marginBottom:10}}>
                        <Button type="primary" onClick={this.handleCheckDelete}>删除</Button>
                    </div>
                    <Table
                        bordered
                        rowSelection = {rowCheckSelection}
                        onRow={(record,index) => {
                            return {
                                onClick: () => { this.handleCheckRowClick(record,index)}
                            }
                        }}
                        columns={columns}
                        dataSource={this.state.dataSource2}
                        pagination={false}
                    />
                </Card>
                <Card title="Mock-分页" style={{margin:"10px 0"}}>
                    <Table
                        bordered
                        columns={columns}
                        dataSource={this.state.dataSource2}
                        pagination={this.state.pagination}
                    />
                </Card>
            </div>
        );
    }
}

export default BasicTable;