import React from 'react';
import { Card, Table, Badge, Modal, message, Button } from 'antd';
import axios from './../../axios/index';

class HighTable extends React.Component{
    state = {
        dataSource:[],     // 数据源
        sortOrder:'',      // 排序方式
    }
    params = {
        page:1
    }

    componentDidMount(){
        const dataSource = [
            {
                id:'0',
                userName:'Jack',
                sex:'1',
                age:4,
                state:'1',
                interest:'1',
                birthday:'2000-01-01',
                address:'杭州市滨江区星光大道',
                time:'09:00'
            },
            {
                id:'1',
                userName:'Tom',
                sex:'1',
                age:1,
                state:'1',
                interest:'2',
                birthday:'2000-01-01',
                address:'杭州市滨江区星光大道',
                time:'09:00'
            },
            {
                id:'2',
                userName:'Lili',
                sex:'1',
                age:6,
                state:'1',
                interest:'3',
                birthday:'2000-01-01',
                address:'杭州市滨江区星光大道',
                time:'09:00'
            },
            {
                id:'3',
                userName:'Lili',
                sex:'1',
                age:8,
                state:'1',
                interest:'4',
                birthday:'2000-01-01',
                address:'杭州市滨江区星光大道',
                time:'09:00'
            },
            {
                id:'4',
                userName:'Lili',
                sex:'1',
                age:7,
                state:'1',
                interest:'5',
                birthday:'2000-01-01',
                address:'杭州市滨江区星光大道',
                time:'09:00'
            },
            {
                id:'5',
                userName:'Lili',
                sex:'1',
                age:9,
                state:'1',
                interest:'6',
                birthday:'2000-01-01',
                address:'杭州市滨江区星光大道',
                time:'09:00'
            },
            {
                id:'6',
                userName:'Lili',
                sex:'1',
                age:3,
                state:'1',
                interest:'2',
                birthday:'2000-01-01',
                address:'杭州市滨江区星光大道',
                time:'09:00'
            },
            {
                id:'7',
                userName:'Lili',
                sex:'1',
                age:6,
                state:'1',
                interest:'2',
                birthday:'2000-01-01',
                address:'杭州市滨江区星光大道',
                time:'09:00'
            },
        ]
        // 为表格的每一行添加key
        dataSource.map((item,index)=>{
            item.key = index;
        });
        this.setState({
            dataSource
        });
    }
    
    // 动态获取mock数据
    request = ()=>{
        axios.ajax({
            url:'/table/list',
            data:{
                params:{
                    page:this.params.page
                },
                // isShowLoading:false
            }
        }).then((res)=>{
            if(res.code === 0){
                // 为表格的每一行添加key
                res.result.list.map((item,index)=>{
                    item.key = index;
                });
                this.setState({
                    dataSource:res.result.list,
                });
            }
        });
    }

    handleChange = (pagination, filters, sorter) =>{
        this.setState({
            sortOrder:sorter.order
        });
    }

    // 删除操作
    handleDelete = (item) => {
        let id = item.id;
        Modal.confirm({
            title:"确认",
            content:"您确认要删除此条数据吗?",
            onOk:()=>{
                message.success("删除成功");
                // this.request();
            }
        });        
    }

    render(){
        const columns1 = [
            {
                title:'id',
                dataIndex:'id',
                width:80
            },
            {
                title:'用户名',
                dataIndex:'userName',
                width:80
            },
            {
                title:'性别',
                dataIndex:'sex',
                width:80,
                render(sex){
                    return sex === 1 ? '男' : '女';
                }
            },
            {
                title:'状态',
                dataIndex:'state',
                width:80,
                render(state){
                    let config = {
                        '1':'咸鱼一条',
                        '2':'风华浪子',
                        '3':'北大才子',
                        '4':'百度FE',
                        '5':'创业者'
                    }
                    return config[state];
                }
            },
            {
                title:'爱好',
                dataIndex:'interest',
                width:80,
                render(interest){
                    let config = {
                        '1':'游泳',
                        '2':'打篮球',
                        '3':'踢足球',
                        '4':'跑步',
                        '5':'爬山',
                        '6':'骑行',
                        '7':'桌球',
                        '8':'麦霸'
                    }
                    return config[interest];
                }
            },
            {
                title:'生日',
                dataIndex:'birthday',
                width:120
            },
            {
                title:'地址',
                dataIndex:'address',
                width:120
            },
            {
                title:'早起时间',
                width:80,
                dataIndex:'time'
            }
        ];
        const columns2 = [
            {
                title:'id',
                dataIndex:'id',
                width:80,
                fixed:'left'
            },
            {
                title:'用户名',
                dataIndex:'userName',
                width:80
            },
            {
                title:'性别',
                dataIndex:'sex',
                width:80,
                render(sex){
                    return sex === 1 ? '男' : '女';
                }
            },
            {
                title:'状态',
                dataIndex:'state',
                width:80,
                render(state){
                    let config = {
                        '1':'咸鱼一条',
                        '2':'风华浪子',
                        '3':'北大才子',
                        '4':'百度FE',
                        '5':'创业者'
                    }
                    return config[state];
                }
            },
            {
                title:'爱好',
                dataIndex:'interest',
                width:80,
                render(interest){
                    let config = {
                        '1':'游泳',
                        '2':'打篮球',
                        '3':'踢足球',
                        '4':'跑步',
                        '5':'爬山',
                        '6':'骑行',
                        '7':'桌球',
                        '8':'麦霸'
                    }
                    return config[interest];
                }
            },
            {
                title:'生日',
                dataIndex:'birthday',
                width:120
            },
            {
                title:'生日',
                dataIndex:'birthday1',
                width:120
            },
            {
                title:'生日',
                dataIndex:'birthday2',
                width:120
            },
            {
                title:'生日',
                dataIndex:'birthday3',
                width:120
            },
            {
                title:'生日',
                dataIndex:'birthday4',
                width:120
            },
            {
                title:'生日',
                dataIndex:'birthda5',
                width:120
            },
            {
                title:'生日',
                dataIndex:'birthday6',
                width:120
            },
            {
                title:'生日',
                dataIndex:'birthday7',
                width:120
            },
            {
                title:'生日',
                dataIndex:'birthday8',
                width:120
            },
            {
                title:'生日',
                dataIndex:'birthday9',
                width:120
            },
            {
                title:'生日',
                dataIndex:'birthday10',
                width:120
            },
            {
                title:'生日',
                dataIndex:'birthday11',
                width:120
            },
            {
                title:'地址',
                dataIndex:'address',
                width:120
            },
            {
                title:'早起时间',
                width:80,
                dataIndex:'time',
                fixed:'right'
            }
        ];
        const columns3 = [
            {
                title:'id',
                dataIndex:'id',
            },
            {
                title:'用户名',
                dataIndex:'userName',
            },
            {
                title:'性别',
                dataIndex:'sex',
                render(sex){
                    return sex === 1 ? '男' : '女';
                }
            },
            {
                title:'年龄',
                dataIndex:'age',
                sorter:(a,b) => {
                    return a.age - b.age;
                },
                sortOrder:this.state.sortOrder
            },
            {
                title:'状态',
                dataIndex:'state',
                render(state){
                    let config = {
                        '1':'咸鱼一条',
                        '2':'风华浪子',
                        '3':'北大才子',
                        '4':'百度FE',
                        '5':'创业者'
                    }
                    return config[state];
                }
            },
            {
                title:'爱好',
                dataIndex:'interest',
                render(interest){
                    let config = {
                        '1':'游泳',
                        '2':'打篮球',
                        '3':'踢足球',
                        '4':'跑步',
                        '5':'爬山',
                        '6':'骑行',
                        '7':'桌球',
                        '8':'麦霸'
                    }
                    return config[interest];
                }
            },
            {
                title:'生日',
                dataIndex:'birthday',
            },
            {
                title:'地址',
                dataIndex:'address',
            },
            {
                title:'早起时间',
                dataIndex:'time'
            }
        ];
        const columns4 = [
            {
                title:'id',
                dataIndex:'id',
            },
            {
                title:'用户名',
                dataIndex:'userName',
            },
            {
                title:'性别',
                dataIndex:'sex',
                render(sex){
                    return sex === 1 ? '男' : '女';
                }
            },
            {
                title:'年龄',
                dataIndex:'age',
            },
            {
                title:'状态',
                dataIndex:'state',
                render(state){
                    let config = {
                        '1':'咸鱼一条',
                        '2':'风华浪子',
                        '3':'北大才子',
                        '4':'百度FE',
                        '5':'创业者'
                    }
                    return config[state];
                }
            },
            {
                title:'爱好',
                dataIndex:'interest',
                render(interest){
                    let config = {
                        '1':<Badge status="success" text="游泳"/>,
                        '2':<Badge status="error" text="打篮球"/>,
                        '3':<Badge status="default" text="踢足球"/>,
                        '4':<Badge status="processing" text="跑步"/>,
                        '5':<Badge status="warning" text="爬山"/>,
                        '6':<Badge status="success" text="骑行"/>,
                        '7':<Badge status="success" text="桌球"/>,
                        '8':<Badge status="success" text="麦霸"/>
                    }
                    return config[interest];
                }
            },
            {
                title:'生日',
                dataIndex:'birthday',
            },
            {
                title:'地址',
                dataIndex:'address',
            },
            {
                title:'操作',
                render:(item)=>{
                    return <Button size="small" type="danger" onClick={()=>{this.handleDelete(item)}}>删除</Button>
                }
            }
        ];
         
        return(
            <div>
                <Card title="头部固定">
                    <Table
                        bordered
                        columns={columns1}
                        dataSource={this.state.dataSource}
                        pagination={false}
                        scroll={{y:240}}
                    />
                </Card>
                <Card title="左侧固定" style={{margin:"10px 0"}}>
                    <Table
                        bordered
                        columns={columns2}
                        dataSource={this.state.dataSource}
                        pagination={false}
                        scroll={{x:2050}}
                    />
                </Card>
                <Card title="表格排序" style={{margin:"10px 0"}}>
                    <Table
                        bordered
                        columns={columns3}
                        dataSource={this.state.dataSource}
                        pagination={false}
                        onChange={this.handleChange}
                    />
                </Card>
                <Card title="操作" style={{margin:"10px 0"}}>
                    <Table
                        bordered
                        columns={columns4}handleDelete
                        dataSource={this.state.dataSource}
                        pagination={false}
                    />
                </Card>
            </div>
        );
    }
}

export default HighTable;