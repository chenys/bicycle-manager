import React from 'react';
import { Card, Button, Table, Form, Select, Modal, message } from 'antd';
import axios from './../../axios/index';
import Utils from './../../utils/utils';
import BaseForm from './../../components/BaseForm';
const FormItem = Form.Item;
const Option = Select.Option;

class City extends React.Component{

    state = {
        list:[],                // 表格信息
        pagination:null,        // 分页信息
        isShowOpenCity:false,   // 显示开通城市弹框
    }

    params = {
        page:1                  // 分页的当前页
    }

    formList = [
        {
            type:'SELECT',
            label:'城市',
            field:'city_id',
            placeholder:'全部',
            initialValue:'0',
            width:100,
            list:[{id:'0',name:'全部'},{id:'1',name:'北京'},{id:'2',name:'天津'},{id:'3',name:'上海'}]
        },
        {
            type:'SELECT',
            label:'用车模式',
            field:'use_mode',
            placeholder:'全部',
            initialValue:'0',
            width:150,
            list:[{id:'0',name:'全部'},{id:'1',name:'指定停车点模式'},{id:'2',name:'禁停区模式'}]
        },
        {
            type:'SELECT',
            label:'营运模式',
            field:'op_mode',
            placeholder:'全部',
            initialValue:'0',
            width:80,
            list:[{id:'0',name:'全部'},{id:'1',name:'自营'},{id:'2',name:'加盟'}]
        },
        {
            type:'SELECT',
            label:'加盟商授权状态',
            field:'auto_status',
            placeholder:'全部',
            initialValue:'0',
            width:100,
            list:[{id:'0',name:'全部'},{id:'1',name:'已授权'},{id:'2',name:'未授权'}]
        },
    ];

    componentDidMount(){
        this.requestList();
    }

    handlefilter = (params) => {
        this.params = params;
        this.requestList();
    }

    // 默认请求接口数据
    requestList = () => {
        axios.requestList(this,'/city/listPage',this.params);
    }

    // 开通城市
    handleOpenCity = () => {
        this.setState({
            isShowOpenCity:true
        });
    }

    // 城市开通提交
    handleSubmit = () => {
        let cityInfo = this.cityForm.props.form.getFieldsValue();
        axios.ajax({
            url:'/city/open',
            data:{
                params:cityInfo
            }
        }).then((res)=>{
            if(res.code == '0'){
                message.success('开通成功');
                this.setState({
                    isShowOpenCity:false
                });
                this.requestList();
            }
        })
    }

    render(){
        const columns = [
            {
                title:'城市ID',
                dataIndex:'id'
            },
            {
                title:'城市名称',
                dataIndex:'name'
            },
            {
                title:'用车模式',
                dataIndex:'use_mode',
                render(mode){
                    return mode === 1 ? '停车点' : '禁停区';
                }
            },
            {
                title:'营运模式',
                dataIndex:'op_mode',
                render(mode){
                    return mode === 1 ? '自营' : '加盟';
                }
            },
            {
                title:'授权加盟商',
                dataIndex:'franchisee_name'
            },
            {
                title:'城市管理员',
                dataIndex:'city_admins',
                render(arr){
                    return arr.map(item => item.user_name).join(',');
                }
            },
            {
                title:'城市开通时间',
                dataIndex:'open_time'
            },
            {
                title:'操作时间',
                dataIndex:'update_time',
                render:Utils.formateDate
            },
            {
                title:'操作人',
                dataIndex:'sys_user_name'
            },
        ];
        return(
            <div>
                <Card>
                    <BaseForm formList={this.formList} filterSubmit={this.handlefilter}/>
                </Card>
                <Card style={{marginTop:10}}>
                    <Button type="primary" onClick={this.handleOpenCity}>开通城市</Button>
                </Card>
                <div className="content-wrap">
                    <Table
                        bordered
                        columns={columns}
                        dataSource={this.state.list}
                        pagination={this.state.pagination}
                    />
                </div>
                <Modal
                    title="开通城市"
                    visible={this.state.isShowOpenCity}
                    onCancel={()=>{
                        this.setState({
                            isShowOpenCity:false
                        })
                    }}
                    onOk={this.handleSubmit}
                >
                    <OpenCityForm wrappedComponentRef={(inst)=>{this.cityForm = inst}}/>
                </Modal>
            </div>
        );
    }
}

class OpenCityForm extends React.Component{
    render(){
        const { getFieldDecorator } = this.props.form;
        const formItemLayou = {
            labelCol:{
                span:5
            },
            wrapperCol:{
                span:10
            }
        }
        return (
            <Form layout="horizontal">
                <FormItem label="选择城市" {...formItemLayou}>
                    {
                        getFieldDecorator('city_id',{
                            initialValue:'1'
                        })(
                            <Select placeholder="请选择">
                                <Option value="1">北京市</Option>
                                <Option value="2">杭州市</Option>
                            </Select>
                        )
                    }
                </FormItem>
                <FormItem label="营运模式" {...formItemLayou}>
                    {
                        getFieldDecorator('op_mode',{
                            initialValue:'1'
                        })(
                            <Select placeholder="请选择">
                                <Option value="1">自营</Option>
                                <Option value="2">加盟</Option>
                            </Select>
                        )
                    }
                </FormItem>
                <FormItem label="用车模式" {...formItemLayou}>
                    {
                        getFieldDecorator('use_mode',{
                            initialValue:'1'
                        })(
                            <Select placeholder="请选择">
                                <Option value="1">指定停车点</Option>
                                <Option value="2">禁停区</Option>
                            </Select>
                        )
                    }
                </FormItem>
            </Form>
        );
    }
}
OpenCityForm = Form.create({})(OpenCityForm);

export default City;