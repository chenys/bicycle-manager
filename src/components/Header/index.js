import React from 'react';
import { Row, Col } from 'antd';
import './index.less';
import Util from '../../utils/utils';
import axios from '../../axios';
import { connect } from 'react-redux';

class Header extends React.Component{

    state={
        userName:'',        // 用户名
        sysTime:'',         // 系统时间
        dayPictureUrl:'',   // 当天的天气图片
        weather:''          // 当天的天气信息
    }

    componentWillMount(){
        this.setState({
            userName:'陈裔松'
        })
        setInterval(()=>{
            let sysTime = Util.formateDate(new Date().getTime());
            this.setState({
                sysTime
            });
        })
        this.getWeatherAPIData();
    }

    getWeatherAPIData(){
        let city='杭州';
        axios.jsonp({
            url:'http://api.map.baidu.com/telematics/v3/weather?location='+encodeURIComponent(city)+'&output=json&ak=3p49MVra6urFRGOT9s8UBWr2'
        }).then((res)=>{
            if(res.status === 'success'){
                let data = res.results[0].weather_data[0];
                this.setState({
                    dayPictureUrl: data.dayPictureUrl,
                    weather: data.weather
                })
            }
        });
    }

    render(){
        const { userName,sysTime,dayPictureUrl,weather } = this.state;
        const menuType = this.props.menuType;

        return (
            <div className="header">
                <Row className="header-top">
                    {
                        !menuType?"":
                        <Col span="6" className="logo">
                            <img src="/assets/logo-ant.svg" alt="logo图片"/>
                            <span>共享单车后台管理系统</span>
                        </Col>
                    }
                    <Col span={menuType?18:24}>
                        <span>欢迎，{userName}</span>
                        <a href="#">退出</a>
                    </Col>
                </Row>
                {
                    menuType?"":
                        <Row className="breadcrumb">
                            <Col span="4" className="breadcrumb-title">
                                {this.props.menuName}
                            </Col>
                            <Col span="20" className="weather">
                                <span className="date">{sysTime}</span>
                                <span className="weather-img">
                                    <img src={dayPictureUrl} alt=""/>
                                </span>
                                <span className="weather-detail">
                                    {weather}
                                </span>
                            </Col>
                        </Row>
                }
            </div>
        );
    }
}

// 获取数据源
const mapStateToProps = state => {
    return {
        menuName:state.menuName
    }
}

export default connect(mapStateToProps)(Header);