import React from 'react';
import { Input, Select, Form, Button, Checkbox, DatePicker } from 'antd';
import Utils from './../../utils/utils';
const FormItem = Form.Item;

class FilterForm extends React.Component{

    initFormList = () => {
        const { getFieldDecorator } = this.props.form;
        const formList = this.props.formList;
        const formItemList = [];

        if(formList && formList.length>0){
            formList.forEach((item,i)=>{
                let type = item.type;
                let label = item.label;
                let field = item.field;
                let placeholder = item.placeholder || '';
                let initialValue = item.initialValue;
                let width = item.width;

                if(item.type == '城市'){
                    const city = <FormItem label="城市" key={"city"}>
                        {
                            getFieldDecorator("city",{
                                initialValue:'0'
                            })(
                                <Select placeholder="全部" style={{width:80}}>
                                    {Utils.getOptionList([{id:'0',name:'全部'},{id:'1',name:'北京'},,{id:'2',name:'上海'},{id:'3',name:'天津'},{id:'4',name:'杭州'},])}
                                </Select>
                            )
                        }
                    </FormItem>
                    formItemList.push(city);
                }
                else if(item.type == '时间查询'){
                    const beginTime = <FormItem label={label} key={"beginTime"}>
                        {
                            getFieldDecorator('beginTime')(
                                <DatePicker showTime placeholder={"开始时间"} format="YYYY-MM-DD HH:mm:ss"/>
                            )
                        }
                    </FormItem>
                    formItemList.push(beginTime);
                    const end_time = <FormItem label="~" colon={false} key={"end_time"}>
                        {
                            getFieldDecorator('end_time')(
                                <DatePicker showTime placeholder={"结束时间"} format="YYYY-MM-DD HH:mm:ss"/>
                            )
                        }
                    </FormItem>
                    formItemList.push(end_time);
                }
                else if(type == 'INPUT'){
                    const input = <FormItem label={label} key={field}>
                        {
                            getFieldDecorator(field,{
                                initialValue:initialValue
                            })(
                                <Input type="text" placeholder={placeholder}/>
                            )
                        }
                    </FormItem>
                    formItemList.push(input);
                }else if(type == 'SELECT'){
                    const select = <FormItem label={label} key={field}>
                        {
                            getFieldDecorator(field,{
                                initialValue:initialValue
                            })(
                                <Select placeholder={placeholder} style={{width:width}}>
                                    {Utils.getOptionList(item.list)}
                                </Select>
                            )
                        }
                    </FormItem>
                    formItemList.push(select);
                }else if(type == 'CHECKBOX'){
                    const checkbox = <FormItem label={label} key={field}>
                        {
                            getFieldDecorator(field,{
                                valuePropName:'checked',    // 必须加上这个属性 
                                initialValue:initialValue   // true | false
                            })(
                                <Checkbox>{label}</Checkbox>
                            )
                        }
                    </FormItem>
                    formItemList.push(checkbox);
                }else if(type == 'DATE'){
                    const date = <FormItem label={label} key={field}>
                        {
                            getFieldDecorator(field)(
                                <DatePicker showTime placeholder={placeholder} format="YYYY-MM-DD HH:mm:ss"/>
                            )
                        }
                    </FormItem>
                    formItemList.push(date);
                }
            });
        }
        return formItemList;
    }

    handleFilterSubmit = () => {
        let fieldsValue = this.props.form.getFieldsValue();
        this.props.filterSubmit(fieldsValue);
    }

    reset = () => {
        this.props.form.resetFields();
    }

    render(){
        return (
            <Form layout="inline">
                { this.initFormList() }
                <FormItem>
                    <Button type="primary" style={{margin:"0 20px"}} onClick={this.handleFilterSubmit}>查询</Button>
                    <Button onClick={this.reset}>重置</Button>
                </FormItem>
            </Form>
        );
    }
}
export default Form.create({})(FilterForm);