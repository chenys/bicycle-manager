import React from 'react';
import { Menu } from 'antd';
import createHistory from "history/createBrowserHistory"
import MenuConfig from './../../config/menuConfig';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { switchMenu } from './../../redux/action';
import './index.less';
const SubMenu = Menu.SubMenu;

class NavLeft extends React.Component{
    state = {
        currentKey:'',
        menuTreeNode:null
    }

    currentTitle = '';

    // 页面刷新的时候，根据路由重新渲染标题
    componentWillMount(){
        const menuTreeNode = this.renderMenu(MenuConfig);
        let currentKey = window.location.hash.replace(/#|\?.*$/g,'');
        this.setState({
            currentKey,
            menuTreeNode
        });

        this.renderTitle(currentKey,MenuConfig);
        this.props.dispatch(switchMenu(this.currentTitle));
    }

    // 路由监听,根据路由变化重新渲染标题和active菜单
    componentDidMount(){
        const history = createHistory();
    
        // Listen for changes to the current location.
        history.listen((location, action) => {
            // location is an object like window.location
            let currentKey = location.hash.replace(/#|\?.*$/g,'');
            this.setState({
                currentKey
            })

            this.renderTitle(currentKey,MenuConfig);
            this.props.dispatch(switchMenu(this.currentTitle));
        })
    }

    // 菜单渲染
    renderMenu = (data) => {
        return data.map(item => {
            if(item.children){
                return(
                    <SubMenu title={item.title} key={item.key}>
                        {this.renderMenu(item.children)}
                    </SubMenu>
                )
            }
            return  <Menu.Item title={item.title} key={item.key}>
                        <NavLink to={item.key} replace>{item.title}</NavLink>
                    </Menu.Item>
        });
    }
    
    // 标题渲染
    renderTitle = (key,data) => {
        data.map(item => {
            if(item.children){
                this.renderTitle(key,item.children);
            }else{
                if(key == item.key){
                    this.currentTitle = item.title;
                }
            }
        });
    }

    render(){
        return (
            <div>
                <div>
                    <div className="logo">共享单车后台管理系统</div>
                </div>
                <Menu 
                    selectedKeys={[this.state.currentKey]}
                    theme="dark"
                >
                    {this.state.menuTreeNode}
                </Menu>
            </div>
        );
    }
}

export default connect()(NavLeft);