import React from 'react';
import { Table } from 'antd';
import Utils from './../../utils/utils';

export default class ETable extends React.Component{
    // 点击表格的行，保存相应的数据
    handleRowClick = (record,index) => {
        let selectedRowKeys = [];
        let selectedRows = [];
        if(this.props.rowSelectionType == 'radio'){
            selectedRowKeys = [index];
            selectedRows = [record];
        }else{
            selectedRowKeys = this.props.selectedRowKeys;
            selectedRows = this.props.selectedRows;

            // 判断当前行是否已经被选中
            let pos = selectedRowKeys.indexOf(index);
            if( pos == -1){
                selectedRowKeys.push(index);
                selectedRows.push(record);
            }else{
                selectedRowKeys.splice(pos,1);
                selectedRows.splice(pos,1);
            }
        }
        this.props.updateSelectedItem(selectedRowKeys,selectedRows);
    }

    // 点击表格的选择按钮，保存相应的数据
    handleSelectChange = (selectedRowKeys,selectedRows)=> {
        this.props.updateSelectedItem(selectedRowKeys,selectedRows);
    }

    // 初始化表格
    tableInit = () => {
        let type = this.props.rowSelectionType;
        let selectedRowKeys = this.props.selectedRowKeys;
        const rowSelection = {
            type,
            selectedRowKeys,
            onChange:this.handleSelectChange
        }

        return <Table
            bordered
            {...this.props}
            rowSelection={type ? rowSelection : null}
            onRow={(record,index) => {
                return {
                    onClick: () => {
                        if(type){
                            this.handleRowClick(record,index)
                        }
                    }
                }
            }}
        />
    }

    render(){
        return (
            <div>
                {this.tableInit()}
            </div>
        );
    }
}