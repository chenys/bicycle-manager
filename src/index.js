import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import Admin from './admin';                             // 没有使用Router之前用这个
// import Home from './pages/route_demo/route1/Home';       // demo1演示用
// import IRouter from './pages/route_demo/route2/router';  // demo2演示用
// import IRouter from './pages/route_demo/route3/router';  // demo3演示用
import Router from './router';
import { Provider } from 'react-redux'; // 用于提供数据源
import configureStore from './redux/store';
import registerServiceWorker from './registerServiceWorker';
const store = configureStore();

// ReactDOM.render(<Admin />, document.getElementById('root'));     // 没有使用Router之前用这个
// ReactDOM.render(<Home />, document.getElementById('root'));      // demo1演示用
// ReactDOM.render(<IRouter />, document.getElementById('root'));   // demo2/3演示用
ReactDOM.render(
    <Provider store={store}>
        <Router />
    </Provider>, 
    document.getElementById('root'));
registerServiceWorker();
