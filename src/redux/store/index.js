// 引入createStore。这是Redux中的一个方法，用于一个数据源对象，保存数据
import { createStore } from 'redux';
import reducer from './../reducer';
import { composeWithDevTools } from 'redux-devtools-extension';

export default () => createStore(reducer,composeWithDevTools())
// export default (prevState) => createStore(reducer,prevState)